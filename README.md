This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

# Configuration

Auth0 needs this rule to add permissions to the IdToken.

```js
function (user, context, callback) {
  const ManagementClient = require('auth0@2.17.0').ManagementClient;
  const management = new ManagementClient({
    token: auth0.accessToken,
    domain: auth0.domain
  });

  const params = { id: user.user_id, page: 0, per_page: 50, include_totals: true };
  management.getUserPermissions(params, function (err, permissions) {
    if (err) {
      // Handle error.
      console.log('err: ', err);
      callback(err);
    } else {
      const permissionsArr = permissions.permissions.filter(p => p.resource_server_identifier === context.request.query.audience).map(p => p.permission_name);

      //console.log(permissionsArr);

      context.idToken["http://permission.namespace/"] = {
        permissions: permissionsArr
      };

      //console.log(context);
      callback(null, user, context);
    }
  });
}
```
