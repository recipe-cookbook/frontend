#!/bin/sh

sed -i 's~window.env={initialized:!1}~window.env={initialized:true,REACT_APP_SENTRY_DSN:"'"$REACT_APP_SENTRY_DSN"'",REACT_APP_BACKEND_URL:"'"$REACT_APP_BACKEND_URL"'",REACT_APP_AUTH0_DOMAIN:"'"$REACT_APP_AUTH0_DOMAIN"'",REACT_APP_AUTH0_CLIENT_ID:"'"$REACT_APP_AUTH0_CLIENT_ID"'",REACT_APP_AUTH0_AUDIENCE:"'"$REACT_APP_AUTH0_AUDIENCE"'"}~g' /usr/share/nginx/html/index.html

# ln -s index.html 404.html

exec "$@"
