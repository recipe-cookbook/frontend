FROM nginx:alpine

EXPOSE 80

WORKDIR /usr/share/nginx/html

COPY ./build/ .
COPY ./entrypoint.sh .

COPY ./nginx.conf /etc/nginx/conf.d/default.conf

ENTRYPOINT [ "sh", "entrypoint.sh" ]

CMD ["nginx", "-g", "daemon off;"]
